# 1 背景

本项目是基于SpringBoot的一个快速开发web项目的功能，本项目在spring-boot的基础上提供了更多的默认配置，以及一些模板化的代码， 简化我们的项目开发

# 2 目标

- 快速集成spring-boot组件,尽量做到只通过引入jar包，0配置基础组件
- 提供日常开发种大部分模板化代码
- 根据日常开发规范，提供更多的默认配置

# 3 快速开始

## 3.0 使用maven 模板快速创建项目

[在idea中使用maven模板创建项目](./doc/在idea中使用maven模板创建项目.md)

## 3.1 应入父模块

```xml

<parent>
    <artifactId>sec-boot-starter-parent</artifactId>
    <groupId>cn.lioyan</groupId>
    <version>${版本号}</version>
</parent>

```

## 3.2 引入相关依赖

```xml

<dependencies>
    <dependency>
        <groupId>cn.lioyan</groupId>
        <artifactId>sec-boot-autoconfigure</artifactId>
    </dependency>

    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>

    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-aop</artifactId>
    </dependency>

    <dependency>
        <groupId>com.github.xiaoymin</groupId>
        <artifactId>knife4j-spring-boot-starter</artifactId>
    </dependency>

</dependencies>

```

## 3.3 编码

创建项目base package,并创建spring-boot 启动类

```java

/**
 * {@link Application}
 *
 * @author cn.lioyan
 * @since 2022/4/22 16:15
 */
@SpringBootApplication
public class Application {
    public static void main(String[] args) {


        SpringApplication.run(Application.class, args);
    }
}

```

创建Controller接口，

```java


@RestController
@RequestMapping(value = "/test")
@Api(tags = "测试API接口")
@Validated
public class Controller {

    final private static Logger logger = LoggerFactory.getLogger(Controller.class);


    final private EsService esService;

    public EsController(EsService esService) {
        this.esService = esService;
    }


    @ResponseBody
    @GetMapping(value = "/")
    @ApiOperation("列表查询")
    public String installBatch(String name) {
        return name;
    }


}
```

在resources 中创建 application.properties ，开启swagger

    #开启swagger
    swagger.enable=true

启动 Application

访问swgger: [Swgger](http://127.0.0.1:8080/doc.html)

# 3 功能说明


## 3.1 统一Controller返回值


## 3.2 统一的异常处理


## 3.3 提供默认的CRUDService与BaseRepository


## 3.4 默认的项目文件路径


## 3.5 默认的日志配置


## 3.6 默认配置H2 数据库


## 3.7 swagger 使用


## 3.8 参数认证


## 3.9 其他扩展功能


## 3.10 其他功能


- 方法执行时间打印



