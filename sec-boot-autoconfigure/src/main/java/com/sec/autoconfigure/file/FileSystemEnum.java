package com.sec.autoconfigure.file;

import com.sec.autoconfigure.file.util.IOUtils;

/**
 * {@link FileSystemEnum}
 *
 * @author com.lioyan
 * @since 2022/4/13 15:14
 */
public interface FileSystemEnum {

    String HDFS = "HDFS";
    String LOCAL = "LOCAL";
    String FTP = "FTP";
    String FASTDFS = "FASTDFS";

}
