package com.sec.autoconfigure.file.exception;

/**
 *
 *  异常状态码
 *
 * {@link ExceptionKeys}
 *
 * @author com.lioyan
 * @since 2022/4/13 15:14
 */
public interface ExceptionKeys {




    int DIR_FILES_ERROR = 410001;


}
