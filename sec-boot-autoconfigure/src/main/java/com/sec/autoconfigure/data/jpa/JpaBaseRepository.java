package com.sec.autoconfigure.data.jpa;

import cn.sec.core.model.base.BaseBean;
import com.sec.autoconfigure.data.BaseRepository;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * {@link JpaBaseRepository}
 *
 * @author cn.lioyan
 * @since 2022/4/14 17:30
 */
public interface JpaBaseRepository<T extends BaseBean> extends JpaRepository<T, Long>, BaseRepository<T> {








}
