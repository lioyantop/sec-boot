## 1 编辑UserArchetypes.xml 文件

路径：C:\Users\Administrator\AppData\Local\JetBrains\IntelliJIdea2021.3\Maven\Indices 

在UserArchetypes.xml文件中添加如下配置(没有文件就新建)

```xml
<archetypes>
    <archetype groupId="cn.lioyan" artifactId="sec-template-archetype" version="0.0.4" />
</archetypes>

```

其中 version 为版本号。




# 2 新建maven项目

![image-20220428093415036](./image/image-20220428093415036.png)

使用maven创建项目，并勾选ceate from archetype

选择 cn.lioyan:sec-template-archetype

之后点击下一步



![image-20220428093520648](./image/image-20220428093520648.png)



填写 项目相关配置，点击Finish完成项目创建





# 3 启动项目



![image-20220428093808020](./image/image-20220428093808020.png)

通过Application 类启动项目



# 4 登录swagger

通过 [http://127.0.0.1:8080/doc.html](http://127.0.0.1:8080/doc.html) 访问swagger
